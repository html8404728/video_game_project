During my last year of High School I learned a few things about informatics and we had to create a video game quite easy because we've never coded before. I created an easy game where the player has to use the right and left arrows to go to the right or left in order to catch "good" avatars (green cubes) and avoid "bad" avatars (red cubes). The player can sees his score and time of game while playing, and when he loses all his lives he can start again by pressing the space key. The player can win additional lives depending on a certain score and if he didn't catch any bad avatars before obtaining this score. With time, there is a speed acceleration of the good and bad avatars coming down so you need to be fast !
You can play the game if you just click on the file and you can see the code if you open it in a software that can open html scripts.

Author : Marion Estoup

E-mail : marion_110@hotmail.fr

June 2015


